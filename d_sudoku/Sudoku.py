from pprint import pprint
import copy


class Sudoku:
    matrix = []

    def __init__(self):
        self.matrix = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
        ]

    def create(self, matrix):
        pprint(matrix)
        letters = {'A': 0, 'B': 1, 'C': 2, 'D': 3,
                   'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8}
        if len(matrix):
            for i in matrix:
                ll = list(i)
                self.matrix[int(letters[ll[0]])][int(ll[1])] = int(matrix[i])

        pprint(self.matrix)
        return self.matrix

    def generate(self):

        # self.matrix = [[(x, y) for x in range(9)] for y in range(9)]
        # pprint(self.matrix)

        self.matrix = [
            [0, 2, 0, 0, 0, 1, 3, 0, 0],
            [7, 6, 0, 4, 0, 0, 1, 0, 0],
            [0, 0, 5, 0, 7, 0, 0, 6, 0],

            [6, 0, 0, 0, 0, 0, 0, 3, 0],
            [0, 0, 0, 0, 0, 7, 0, 4, 0],
            [5, 0, 0, 0, 1, 0, 0, 0, 9],

            [0, 0, 0, 0, 3, 2, 0, 0, 0],
            [0, 9, 0, 0, 0, 0, 0, 0, 8],
            [0, 8, 4, 0, 0, 0, 0, 0, 0],
        ]

        pprint(self.matrix)

        # self.matrix = [
        #     [0, 0, 0, 1, 0, 2, 0, 0, 0],
        #     [1, 6, 0, 0, 0, 0, 3, 7, 0],
        #     [5, 0, 0, 7, 0, 4, 0, 0, 9],
        #
        #     [0, 0, 6, 0, 0, 9, 0, 8, 0],
        #     [0, 0, 0, 2, 5, 6, 0, 0, 3],
        #     [3, 4, 0, 0, 0, 0, 0, 0, 0],
        #
        #     [0, 0, 0, 9, 0, 0, 4, 0, 0],
        #     [2, 5, 4, 0, 0, 0, 0, 1, 7],
        #     [0, 1, 0, 4, 0, 0, 0, 5, 8],
        # ]

        # self.matrix = [
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
        # ]

    def single(self, x, y):
        m = self.matrix

        if m[y][x] != 0 and type(m[y][x]) != set:
            return

        possible = {1, 2, 3, 4, 5, 6, 7, 8, 9}

        for xa in range(9):
            # print(m[y][xa], '>>')
            if m[y][xa] in possible:
                possible.remove(m[y][xa])

        for ya in range(9):
            # print(m[ya][x], '<<')
            if m[ya][x] in possible:
                possible.remove(m[ya][x])

        # internal matrix factor
        xf = int(x / 3) * 3
        yf = int(y / 3) * 3

        for ry in range(3):
            for rx in range(3):
                # print(m[ry + yf][rx + xf])
                if m[ry + yf][rx + xf] in possible:
                    possible.remove(int(m[ry + yf][rx + xf]))

        return possible

    def scan(self):
        print('[scan ******]')
        m = self.matrix
        ll = []

        rescan = False

        for y in range(9):
            for x in range(9):
                if m[y][x] == 0 or type(m[y][x]) is set:
                    pos = self.single(x, y)
                    ll.append({'c': {'y': y, 'x': x}, 'p': pos})
                    if len(pos) == 1:
                        self.matrix[y][x] = iter(pos).__next__()
                        pprint(f"-> [{x}][{y}] {pos}")
                        rescan = True
                    elif len(pos) > 1:
                        self.matrix[y][x] = pos

        ll.sort(key=lambda z: len(z['p']))

        if rescan:
            print('[rescan ******]')
            self.scan()
        elif len(ll) > 0:
            self.diff(ll)
            """ print('[uniq ******]')
            res = 0
            for i in range(9):
                if self.uniq(i, i):
                    res += 1
            if res > 0:
                pprint('[uniq succes ******]')
                self.scan()
            else:
                pprint(ll) """

        # pprint(self.matrix)
        print('[scanned ******]')

    """ TODO: Completar algoritmo de diferencia de sets """

    def diff(self, ll):
        pass

        """ def diffrow(y, c):
            for x in self.matrix[y]:
                if type(self.matrix[y][x]) is set:
                    print("difference:")
                    pprint(self.matrix[y][x].difference(c))

        def diffcolumn(x, c):
            pass

        for a in ll:
            if len(a['p']) == 2:
                for b in ll:
                    if a == b:
                        continue
                    elif a['p'] == b['p'] and (a['c']['x'] == b['c']['x'] or a['c']['y'] == b['c']['y']):
                        if a['c']['y'] == b['c']['y']:
                            diffrow(a['c']['y'], a['p'])
                        else:
                            diffcolumn(a['c']['x'], a['p'])

            elif len(a['p']) > 2:
                return """

    # https://www.kristanix.com/sudokuepic/sudoku-solving-techniques.php

    def uniq(self, x, y):
        m = self.matrix.copy()

        row = []
        row_totals = {}
        column = []
        column_totals = {}

        res = False

        for xa in range(9):
            if type(m[y][xa]) is set:
                row.append({'c': {'x': xa, 'y': y}, 'p': m[y][xa]})
                for a in m[y][xa]:
                    if a in row_totals:
                        row_totals[a] += 1
                    else:
                        row_totals[a] = 1

        for ya in range(9):
            if type(m[ya][x]) is set:
                column.append({'c': {'x': x, 'y': ya}, 'p': m[ya][x]})
                for a in m[ya][x]:
                    if a in column_totals:
                        column_totals[a] += 1
                    else:
                        column_totals[a] = 1

        """ for num, appear in row_totals.items():
            if appear == 1:
                for ir in row:
                    if num in ir['p']:
                        pprint(f"-> [{ir['c']['y']}][{ir['c']['x']}] {num}")
                        self.matrix[ir['c']['y']][ir['c']['x']] = num
                        res = True """

        """ for num, appear in column_totals.items():
            if appear == 1:
                for ic in column:
                    if num in ic['p']:
                        pprint(f"-> [{ic['c']['y']}][{ic['c']['x']}] {num}")
                        self.matrix[ic['c']['y']][ic['c']['x']] = num
                        res = True """

        return res

    def uniq_quadrant(self, qx, qy):
        m = self.matrix.copy()

        quadrant = []
        quadrant_totals = {}

        # res = False

        for ry in range(3):
            for rx in range(3):
                y = (3 * qy) + ry
                x = (3 * qx) + rx
                if type(m[y][x]) is set:
                    quadrant.append({'c': {'x': x, 'y': y}, 'p': m[y][x]})
                    for a in m[y][x]:
                        if a in quadrant_totals:
                            quadrant_totals[a] += 1
                        else:
                            quadrant_totals[a] = 1

        pprint(quadrant)
        pprint(quadrant_totals)
        #
        # for num, appear in quadrant_totals.items():
        #     if appear == 1:
        #         for i in quadrant:
        #             if num in i['p']:
        #                 pprint(f"-> [{i['c']['x']}][{i['c']['y']}] {num}")
        #                 self.matrix[i['c']['y']][i['c']['x']] = num
        #                 res = True
        #
        # return res

    def print_matrix(self):
        m = copy.deepcopy(self.matrix)
        for y in m:
            for i in range(9):
                if type(y[i]) is set:
                    y[i] = 0

        pprint(self.matrix)
        pprint(m)
