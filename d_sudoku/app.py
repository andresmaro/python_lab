from flask import Flask, jsonify, request
from flask_cors import CORS
from .Sudoku import Sudoku

app = Flask(__name__)
CORS(app)
print(__name__)

sudoku = Sudoku()


@app.route('/')
def helloworld():
    res = {'succes': True, 'message': 'Sudoku API'}
    return jsonify(res)


@app.route('/create/', methods=['POST'])
def create():
    sudoku.create(request.get_json())
    sudoku.scan()

    res = {}

    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    ll = 0

    for line in sudoku.matrix:
        n = 0
        for x in line:
            if type(x) is set:
                line[n] = list(x)

            res[letters[ll] + n.__str__()] = line[n]

            n += 1
        ll += 1

    return jsonify(res)


@app.route('/restart/')
def restart():
    sudoku.__init__()
    return "OK"


app.run(port=5000)
