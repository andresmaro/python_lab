import sys


def read():
    numbers = []
    with open('resources/numbers.txt', 'r', encoding='utf-8') as f:
        for line in f:
            try:
                numbers.append(int(line))
            except ValueError:
                continue

    print(numbers)


def write():
    nums = input('Please enter a comma separated series of numbers')
    nums = nums.split(',')
    mode = input('Press w for overwrite the file, leave blank to append')
    print(mode)

    with open('resources/numbers.txt', 'w' if mode == 'w' else 'a', encoding='utf-8') as f:
        for num in nums:
            f.write(f'{num}\n')
