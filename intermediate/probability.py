from random import uniform, random
from numpy.random import choice


# Basic Probability

def flips(n):
    flips = sum(1 for _ in [uniform(0, 1) for _ in range(n)] if y > 0.5)
    return flips / n


def prob(n):
    flips = [uniform(0, 1) for _ in range(n)]
    return sum(flips) / n


def r_prob(n):
    flips = [random() for _ in range(n)]
    return sum(flips) / n


# With Sampling - Numpy lib - more accurate

def samp_prob(n):
    space = [0, 1]  # sample spaces: si/no coin flip
    flips = choice(space, n, replace=True, p=[0.5, 0.5])  # p: probabilidad para cada sample point
    return sum(flips) / n
