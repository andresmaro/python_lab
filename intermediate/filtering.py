from pprint import pprint

from resources.data import DATA


def show_data():
    pprint(DATA)
    pass


def show_languages():
    print([item['language'] for item in DATA])


def filter_by_language(lang):
    pprint([(item['name'], item['language']) for item in DATA if item['language'] == lang])
