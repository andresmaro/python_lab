"""Model for aircrafts flights"""


class Aircraft:
    def __init__(self, model, num_rows, num_seats_per_row):
        self._model = model
        self._num_rows = num_rows
        self._num_seats_per_row = num_seats_per_row

    def seating_plan(self):
        return (range(1, self._num_rows + 1), "ACDEFGHK"[:self._num_seats_per_row])


class Flight:

    def __init__(self, number, aircraft: Aircraft):
        self._number = number
        self._aircraft = aircraft

        rows, seats = self._aircraft.seating_plan()
        self._seating = [None] + [{letter: None for letter in seats} for _ in rows]

    def number(self):
        return self._number

    def aircraft_model(self):
        return self._aircraft._model

    def allocate_seat(self, seat, passenger):
        """Allocate a seat to passenger

        Args:
            seat: A seat designator such as '12C' or '2F'.
            passenger: The passenger name.

        Raises:
            ValueError: If seat is unavailable.

        """

        rows, seat_letters = self._aircraft.seating_plan()

        letter = seat[-1]
        if letter not in seat_letters:
            raise ValueError(f"Invalid seat letter {letter}")

        row_text = seat[:-1]
        try:
            row = int(row_text)
        except ValueError:
            raise ValueError(f"Invalid seat row {row_text}")

        if row not in rows:
            raise ValueError(f"Invalid row number {row}")

        if self._seating[row][letter] is not None:
            raise ValueError(f"Seat {seat} already occupied")

        self._seating[row][letter] = passenger


    def available_seats(self):
        return sum(sum(1 for s in row.values() if s is None) for row in self._seating if row is not None)


def makeF():
    f = Flight("AV145", Aircraft("A330-300", 65, 6))
    return f

# Testing: In PyCharm > Tools > Python Console
# >>> from from c_classes import makeF
# >>> f = makeF()
# Test the class methods...

# Further options:
# @staticmethod: def method() with no self parameter
# @classmethod: def method(cls) with class parameter, referencing
