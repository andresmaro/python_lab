import socket


class Resolver:
    def by_name(self, domain):
        print('by name')
        return socket.gethostbyname(domain)

    def by_ip(self, ip, type=0):
        print('by ip')
        return socket.gethostbyaddr(ip)

# run:
# >>> import a_hello_pack.socket as hp
# >>> s = hp.Resolver()
# >>> s.by_name('google.com')