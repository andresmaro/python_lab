from flask import Flask, jsonify, render_template
from flask_pymongo import PyMongo

app = Flask(__name__,
            static_url_path='',
            static_folder='web/static',
            template_folder='web/templates')
app.config["MONGO_URI"] = "mongodb://maro:123456@maro-flask-mongo:27017/flask?authSource=admin"
mongo = PyMongo(app)


@app.route("/")
def home():
    return "Server running"


@app.route("/jinja")
def jinja():
    return render_template('jinja.html', my_string="Wheeeee!", my_list=[0, 1, 2, 3, 4, 5])


@app.route("/socket")
def socket():
    return render_template('socket.html')


@app.route('/api/test')
def init():
    test = mongo.db.test.find()
    res = []
    for t in test:
        res.append({'name': t['name'], 'phone': t['phone']})
    return jsonify(res)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port="5000")
