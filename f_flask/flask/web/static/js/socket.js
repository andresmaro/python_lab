import * as messages from './messages.js'

let socket = new WebSocket("ws://ws.nix:5001");

socket.onopen = function () { messages.pushMessage('Socket connected', 'positive') }
socket.onclose = function () { messages.pushMessage('Socket disconnected', 'error') }
socket.onmessage = function (raw) {
  const event = JSON.parse(raw.data)
  console.log(event)
  switch (event.type) {
    case 'users':
      document.getElementById('users-count').innerHTML = `Users: ${event.count}`
      break;
    case 'message':
      messages.pushMessage(`Message from server: ${event.value}`, 'info')
      break;
    default:
      break;
  }
}

document.addEventListener('DOMContentLoaded', function (event) {
  messages.pushMessage('DOM complete')

  document.getElementById('send-but').addEventListener('click', function (event) {
    const message = document.getElementById('message-input')
    socket.send(JSON.stringify({ action: 'message', payload: message.value }));
  })

  document.getElementById('clear-but').addEventListener('click', function (event) {
    document.getElementById('messages').innerHTML = ''
  })

  const buttons = document.getElementsByClassName('ui message')

  for (let i = 0; i < buttons.length; i++) {
    const element = buttons[i];
    element.addEventListener('click', () => {
      element.style['display'] = 'none'
    })
  }
})