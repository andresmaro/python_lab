
const container = document.getElementById('messages');

export function pushMessage(message, type = "info") {

  let element = document.createElement('div')
  element.className = `ui tiny message ${type}`
  element.innerHTML = message
  element.addEventListener('click', () => {
    element.style['display'] = 'none'
  })
  container.appendChild(element)

}

