import asyncio
import json

import websockets

print("socket start")

# STATE = []

USERS = set()


# def state_event():
#     return json.dumps({"type": "state", "state": STATE})


def users_event():
    return json.dumps({"type": "users", "count": len(USERS)})


async def notify_state(message):
    if USERS:
        event = json.dumps({"type": "message", "value": message})
        await asyncio.wait([user.send(event) for user in USERS])


async def notify_users():
    if USERS:
        message = users_event()
        await asyncio.wait([user.send(message) for user in USERS])


async def register(websocket):
    USERS.add(websocket)
    await notify_users()


async def unregister(websocket):
    USERS.remove(websocket)
    await notify_users()


async def worker(websocket, path):
    print('New Connection')
    await register(websocket)
    try:
        # await websocket.send(state_event())
        async for message in websocket:
            data = json.loads(message)
            if data["action"] == "message":
                # STATE.append(data['payload'])
                await notify_state(data['payload'])
            elif data["action"] == "message":
                STATE["value"] += 1
                await notify_state()
            else:
                logging.error("unsupported event: {}", data)
    finally:
        await unregister(websocket)

start_server = websockets.serve(worker, "0.0.0.0", 5001)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
