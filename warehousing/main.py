import os

from dotenv import load_dotenv
from google.cloud import bigquery

from src.big_query import BigQuery

load_dotenv()


def main():
    client = bigquery.Client()
    BigQuery(client, os.environ.get('TABLE_NAME'))

    # schema = [
    #     bigquery.SchemaField("full_name", "STRING", mode="REQUIRED"),
    #     bigquery.SchemaField("age", "INTEGER", mode="REQUIRED"),
    # ]
    #
    # table = bigquery.Table('andresmaro.maro_dataset.test_1', schema=schema)
    # table = client.create_table(table)  # Make an API request.
    # print(
    #     "Created table {}.{}.{}".format(table.project, table.dataset_id, table.table_id)
    # )


if __name__ == '__main__':
    main()
