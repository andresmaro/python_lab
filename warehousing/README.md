# Data warehousing project

## Steps

1) Read the docs https://googleapis.dev/python/bigquery/latest/index.html
2) Get json credentials and save them to ./config/google_credentials.json
3) Run main.py and follow the instructions

#### LINKS
+ BigQuery Samples: https://cloud.google.com/bigquery/docs/samples
