import faker
import random
from google.cloud.exceptions import NotFound
from google.cloud import bigquery

menu = """
Select an option:
1: Verify table existence OR create a new one
2: Insert items
3: Read items
"""


class BigQuery:
    def __init__(self, client, table_id):
        self.client = client
        self.table_id = table_id
        self.fake = faker.Faker()
        print("\n\x1b[36m BIG QUERY LAB \x1b[0m")
        self.menu()

    def menu(self):
        options = range(0, 5)
        option = int(input(menu))

        if option not in options:
            print('Please select a valid option')
        else:
            if option == 1:
                self.existence()
            elif option == 2:
                self.insert_items()
            elif option == 3:
                self.read_items()

    def existence(self):
        try:
            self.client.get_table(self.table_id)
            print('Table {} already exists.'.format(self.table_id))
        except NotFound:
            print('Table {} is not found.'.format(self.table_id))
            do_create = input('Do you want to create it?')
            if do_create == 'y':
                self.create()
            else:
                self.menu()

    def create(self):
        schema = [
            bigquery.SchemaField("name", "STRING", mode="REQUIRED"),
            bigquery.SchemaField("user_name", "STRING", mode="REQUIRED"),
            bigquery.SchemaField("email", "STRING", mode="REQUIRED"),
            bigquery.SchemaField("age", "INTEGER", mode="REQUIRED"),
            bigquery.SchemaField("address", "STRING", mode="REQUIRED"),
            bigquery.SchemaField("is_client", "BOOLEAN", mode="REQUIRED"),
            bigquery.SchemaField("last_login", "DATE", mode="REQUIRED"),
            bigquery.SchemaField("last_buy", "FLOAT", mode="REQUIRED"),
        ]

        table = bigquery.Table(self.table_id, schema=schema, )
        table = self.client.create_table(table)  # Make an API request.
        print(
            "Created table {}.{}.{}".format(table.project, table.dataset_id, table.table_id)
        )
        self.menu()

    def insert_items(self):
        num_items = int(input('How many items do you want to insert?'))

        def item():
            profile = self.fake.profile('name', 'email', 'user_name')
            return {
                u"name": self.fake.name(),
                u"user_name": self.fake.user_name(),
                u"email": self.fake.email(),
                u"age": random.randint(20, 60),
                u"address": self.fake.address(),
                u"is_client": self.fake.boolean(),
                u"last_login": self.fake.date_this_month().__str__(),
                u"last_buy": random.random() * random.randint(1, 100),
            }

        rows_to_insert = [item() for i in range(0, num_items)]
        errors = self.client.insert_rows_json(self.table_id, rows_to_insert)  # Make an API request.
        if not errors:
            print("New rows have been added.")
        else:
            print("Encountered errors while inserting rows: {}".format(errors))
        self.menu()

    def read_items(self):
        rows_iter = self.client.list_rows(self.table_id, max_results=10)
        rows = list(rows_iter)

        for item in rows:
            print(item)
