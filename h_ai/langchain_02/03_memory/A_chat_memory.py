import os
from pprint import pprint

import dotenv
from langchain.memory import ChatMessageHistory
from langchain_openai import ChatOpenAI

dotenv.load_dotenv()

llm = ChatOpenAI(openai_api_key=os.getenv("OPENAI_API_KEY"))

history = ChatMessageHistory()
history.add_ai_message("Hi! Ask me anything about LangChain.")
history.add_user_message("Describe a metaphor for learning LangChain in one sentence.")
response = llm.invoke(history.messages)
history.add_ai_message(response.content)

history.add_user_message("Summarize the preceding sentence in fewer words")
response = llm.invoke(history.messages)
history.add_ai_message(response.content)

pprint(history.messages)



