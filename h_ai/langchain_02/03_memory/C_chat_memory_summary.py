import os
from pprint import pprint

import dotenv
from langchain.chains import ConversationChain
from langchain.memory import ConversationSummaryMemory
from langchain_openai import ChatOpenAI

dotenv.load_dotenv()

llm = ChatOpenAI(openai_api_key=os.getenv("OPENAI_API_KEY"))

memory = ConversationSummaryMemory(llm=llm)

conversation = ConversationChain(
    llm=llm, 
    memory=memory,
    verbose=True
)

conversation.predict(input="Hi, I'm John. I'm planning a trip to Paris.")
conversation.predict(input="I'm interested in art and history. Any recommendations?")
conversation.predict(input="That sounds great! How many days should I plan for?")

pprint(memory.predict_new_summary(
    messages=memory.chat_memory.messages,
    existing_summary=""
))


