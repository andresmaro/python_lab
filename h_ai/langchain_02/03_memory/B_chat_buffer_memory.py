import os
from pprint import pprint

import dotenv
from langchain.chains import ConversationChain
from langchain.memory import ConversationBufferWindowMemory
from langchain_openai import ChatOpenAI

dotenv.load_dotenv()

llm = ChatOpenAI(openai_api_key=os.getenv("OPENAI_API_KEY"))

# This will keep the 2 most recent exchanges
memory = ConversationBufferWindowMemory(k=2)

conversation = ConversationChain(
    llm=llm,
    memory=memory,
    verbose=True
)
# Start the conversation
response1 = conversation.predict(input="Hi, my name is Alice.")

# Continue the conversation
response2 = conversation.predict(input="What's my name?")

# Add more interactions
response3 = conversation.predict(input="Tell me a fun fact about programming.")

# Ask about a previous topic
response4 = conversation.predict(input="What was the fun fact you just told me?")

response5 = conversation.predict(input="What's my name?")

pprint(conversation.memory.chat_memory.messages)




