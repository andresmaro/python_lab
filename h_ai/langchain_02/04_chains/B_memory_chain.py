import os
from pprint import pprint
from typing import Dict, Any
import dotenv
from langchain.memory import SimpleMemory
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import PromptTemplate
from langchain_openai import ChatOpenAI

dotenv.load_dotenv()

llm = ChatOpenAI(openai_api_key=os.getenv("OPENAI_API_KEY"))

memory = {}

title_template = PromptTemplate(
    input_variables=['genre'],
    template='Generate a movie title for a {genre} film:',
)

synopsis_template = PromptTemplate(
    input_variables=['title'],
    template='Write a brief synopsis for a movie titled "{title}":',
)

genre_tags_template = PromptTemplate(
    input_variables=['title', 'synopsis'],
    template='Suggest 3-5 genre tags for a movie title "{title}" based on the synopsis: {synopsis}'
)


def update_memory(res):
    pprint(res)
    memory[res["tag"]] = res["response"].content
    return memory


movie_chain = (
        title_template
        | llm
        | (lambda x: update_memory({"tag": "title", "response": x}))
        | synopsis_template
        | llm
        | (lambda x: update_memory({"tag": "synopsis", "response": x}))
        | genre_tags_template
        | llm
        | (lambda x: update_memory({"tag": "genre", "response": x}))
)

result = movie_chain.invoke({"genre": "action"})
print(memory)
