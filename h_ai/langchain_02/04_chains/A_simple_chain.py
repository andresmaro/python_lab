import os

import dotenv
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import PromptTemplate
from langchain_openai import ChatOpenAI

dotenv.load_dotenv()

llm = ChatOpenAI(openai_api_key=os.getenv("OPENAI_API_KEY"))

title_template = PromptTemplate(
    input_variables=['genre'],
    template='Generate a movie title for a {genre} film:',
)

synopsis_template = PromptTemplate(
    input_variables=['content'],
    template='Write a brief synopsis for a movie titled "{content}":',
)

genre_tags_template = PromptTemplate(
    input_variables=['content'],
    template='Suggest 3-5 genre tags for "{content}" based on the previous synopsis:'
)


def print_output(x):
    print(x)
    return x


movie_chain = (
        title_template
        | llm
        | (lambda x: print_output(x))
        | StrOutputParser()
        | synopsis_template
        | llm
        | (lambda x: print_output(x))
        | StrOutputParser()
        | genre_tags_template
        | llm
        | StrOutputParser()
)

result = movie_chain.invoke({"genre": "comedy"})
print(result)
