import os

import dotenv
from langchain_core.prompts import PromptTemplate
from langchain_community.llms import HuggingFaceHub
from langchain_community.llms import HuggingFaceEndpoint

dotenv.load_dotenv()

hug_key = os.getenv("HUGGINGFACE_KEY")

template = "You are an artificial intelligence assistant, answer the question: {question}"
prompt_template = PromptTemplate(template=template, input_variables=["question"])

print(prompt_template.invoke({"question": "What is LangChain?"}))

llm = HuggingFaceEndpoint(
    repo_id='tiiuae/falcon-7b-instruct',
    huggingfacehub_api_token=hug_key
)
llm_chain = prompt_template | llm
question = "What is LangChain?"

print(llm_chain.invoke({"question": question}))
