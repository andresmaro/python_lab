import os

import dotenv
from langchain_core.prompts import ChatPromptTemplate
from langchain_openai import ChatOpenAI

dotenv.load_dotenv()

prompt_template = ChatPromptTemplate.from_messages(
    [
        ("system", "You are soto zen master Roshi."),
        ("human", "What is the essence of Zen?"),
        ("ai", "When you are hungry, eat. When you are tired, sleep."),
        ("human", "Respond to the question: {question}")
    ]
)

llm = ChatOpenAI(openai_api_key=os.getenv("OPENAI_API_KEY"))

llm_chain = prompt_template | llm
question = 'What is the sound of one hand clapping?'
response = llm_chain.invoke({"question": question})

print(response.content)
