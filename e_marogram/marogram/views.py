# Utilities
from datetime import datetime as dt

from django.http import HttpResponse, JsonResponse


def home(request):
    return HttpResponse('Marogram Project')


def hw(request):
    now = dt.now().strftime('%m/%d/%Y %H:%M')
    return HttpResponse(f'Hey it\'s {now}')


def sum(request):
    # http://127.0.0.1:8000/calc?nums=1,2,3,5
    nums = [int(i) for i in request.GET['nums'].split(',')]
    return JsonResponse({'sum': sum(nums)})


def args(request, name, phone):
    # http://127.0.0.1:8000/args/andres/3001100000
    return JsonResponse({'status': 'ok', 'name': name, 'phone': phone})
