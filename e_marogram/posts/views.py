from django.shortcuts import render

posts_data = {
    'posts': [
        {'id': 1, 'title': 'My first post', 'content': 'This is my fancy new post'},
        {'id': 2, 'title': 'My second post', 'content': 'This is my fancy new post'},
        {'id': 3, 'title': 'My third post', 'content': 'This is my fancy new post'}
    ],
}


def posts(request):
    return render(request, 'feed.html', posts_data)
 