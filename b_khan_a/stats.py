import math
import statistics
from collections import OrderedDict


def mean(l: list):
    """media"""
    sum = math.fsum(l)
    return {"a": sum / len(l), "b": statistics.mean(l)}


def median(l: list):
    """mediana"""

    l.sort()
    m = len(l) / 2

    if m % 1 == 0:
        val = (l[int(m) - 1] + l[int(m)]) / 2
    else:
        val = l[int(m) - 1]

    return {"a": val, "b": statistics.median(l)}


def mode(l: list):
    """moda"""
    o = {}
    for n in l:
        o.update({n: o[n] + 1}) if n in o else o.update({n: 1})
    ord = OrderedDict(sorted(o.items(), key=lambda t: t[1]))
    val = list(ord.keys())[len(ord) - 1]

    return {"a": val, "b": statistics.mode(l)}
