def com_div(a, b, c):
    """Lista de común divisor entre tres numeros."""
    n = 1;
    while n <= a / 2 and n <= b / 2 and n <= c / 2:
        if a / n % 1 == 0 and b / n % 1 == 0 and c / n % 1 == 0:
            print('>', [n, a / n, b / n, c / n])
        n += 1


def m_com_mul(a, b):
    """Mínimo común múltiplo"""
    if a == b:
        return a
    may = a if a > b else b
    men = b if a > b else a
    n = 1
    while True:
        x = may * n
        if (x / men) % 1 == 0:
            return x
        n += 1


def is_prime(n):
    """Devuelve si un número es primo o no"""
    if n == 1:
        return False
    if n == 2 or n == 3:
        return True
    if n % 2 == 0:
        return False
    if n % 3 == 0:
        return False

    i = 5
    w = 2

    while i * i <= n:
        if n % i == 0:
            return False

        i += w
        w = 6 - w

    return True


def get_primes(n):
    """genera un listado de números primos hasta determinado número"""
    i = 1
    t = 0
    a = []
    while i <= n:
        if is_prime(i):
            a.append(i)
            t += 1
        i += 1
    print(f'total: {t} {t / n * 100}%')
    return a


def factor_p(n):
    """Devuelve una lista de factores primos de n ."""
    a = []
    f = 2  # Primer factor posible
    while n > 1:  # Mientras tenga algún factor...
        print(f)
        if is_prime(f):
            if n % f == 0:
                a.append(f)
                n /= f
            else:
                f += 1
        else:
            f += 1

    return a
