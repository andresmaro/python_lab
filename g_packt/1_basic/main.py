import time
import progressbar


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    # create a progress bar widget with a maximum value of 100
    bar = progressbar.ProgressBar(max_value=100)

    # loop through some simulated work
    for i in range(100):
        # update the progress bar
        bar.update(i)
        # simulate some work
        time.sleep(0.1)

    # finish the progress bar
    bar.finish()

    print (f"Success at: {name}")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('Hello Maro')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
