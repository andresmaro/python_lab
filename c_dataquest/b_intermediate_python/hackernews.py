import datetime as dt
from csv import reader
import os

script_dir = os.path.dirname(__file__)
rel_path = "data/hacker_news.csv"
file = open(os.path.join(script_dir, rel_path), encoding="utf8")

read_file = reader(file)
hn = list(read_file)

headers = hn[0:1]
hn = hn[1:]

ask_posts = []
show_posts = []
other_posts = []

for row in hn:
    title = row[1]
    if title.lower().startswith('ask hn'):
        ask_posts.append(row)
    elif title.lower().startswith('show hn'):
        show_posts.append(row)
    else:
        other_posts.append(row)

total_ask_comments = 0
for row_ask in ask_posts:
    total_ask_comments += int(row_ask[4])

avg_ask_comments = total_ask_comments / len(ask_posts)

total_show_comments = 0
for row_show in show_posts:
    total_show_comments += int(row_show[4])

avg_show_comments = total_show_comments / len(show_posts)

print(f"ASK average comments {avg_ask_comments}")
print(f"SHOW average comments {avg_show_comments}")

result_list = []
for row in ask_posts:
    result_list.append([row[6], int(row[4])])

counts_by_hour = {}
comments_by_hour = {}

for row in result_list:
    date = dt.datetime.strptime(row[0], '%m/%d/%Y %H:%M')
    hour = date.strftime('%H')
    if hour in counts_by_hour:
        counts_by_hour[hour] += 1
        comments_by_hour[hour] += row[1]
    else:
        counts_by_hour[hour] = 1
        comments_by_hour[hour] = row[1]

avg_by_hour = []

for count in counts_by_hour:
    avg_by_hour.append([count, comments_by_hour[count] / counts_by_hour[count]])

avg_by_hour.sort(key=lambda x: x[1], reverse=True)

print("Las 5 horas con mayor promedio de comentarios en ASK")
for i in avg_by_hour[:5]:
    print("{}:00: {:.2f}".format(i[0], i[1]))
