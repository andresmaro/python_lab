# %%
import seaborn as sns
import os

import pandas as pd
import numpy as np

try:
    os.chdir(os.path.join(os.getcwd(), 'c_dataquest/g_data_cleaning'))
    print(os.getcwd())
except:
    pass

# %%
happiness2015 = pd.read_csv("data/World_Happiness_2015.csv")
happiness2016 = pd.read_csv("data/World_Happiness_2016.csv")
happiness2017 = pd.read_csv("data/World_Happiness_2017.csv")

shape_2015 = happiness2015.shape
shape_2016 = happiness2016.shape
shape_2017 = happiness2017.shape

# %%
# Unificamos nombres de las columnas

cols = [happiness2015.columns.tolist(), happiness2016.columns.tolist(),
        happiness2017.columns.tolist()]

happiness2015.columns = happiness2015.columns.str.replace(
    r'[\(\)]', '').str.strip().str.lower().str.replace(' ', '_')

happiness2016.columns = happiness2016.columns.str.replace(
    r'[\(\)]', '').str.strip().str.lower().str.replace(' ', '_')

happiness2017.columns = happiness2017.columns.str.replace(
    '.', ' ').str.replace('\s+', ' ').str.strip().str.lower().str.replace(' ', '_')


cols2 = [happiness2015.columns.tolist(), happiness2016.columns.tolist(),
         happiness2017.columns.tolist()]

# %%
# Igualamos paises en los 3 dataframes.

drops = ['Taiwan Province of China', 'Hong Kong S.A.R., China']

h15 = pd.concat([happiness2015, happiness2016, happiness2017], sort=False)
h15 = h15[happiness2015.columns]
h15.iloc[158:, 2:] = np.NaN  # from shape_2015
h15.drop_duplicates(subset='country', inplace=True)
h15.reset_index(drop=True, inplace=True)
h15 = h15[~h15.country.isin(drops)]
h15['year'] = 2015

h16 = pd.concat([happiness2016, happiness2015, happiness2017], sort=False)
h16 = h16[happiness2016.columns]
h16.iloc[157:, 2:] = np.NaN  # from shape_2016
h16.drop_duplicates(subset='country', inplace=True)
h16.reset_index(drop=True, inplace=True)
h16 = h16[~h16.country.isin(drops)]
h16['year'] = 2016


h17 = pd.concat([happiness2017, happiness2015, happiness2016], sort=False)
h17 = h17[happiness2017.columns]
h17.iloc[155:, 2:] = np.NaN  # from shape_2017
h17.drop_duplicates(subset='country', inplace=True)
h17.reset_index(drop=True, inplace=True)
h17 = h17[~h17.country.isin(drops)]
h17['year'] = 2017

# %%

happiness2015 = h15
happiness2016 = h16
happiness2017 = h17

# %%
missing_2015 = happiness2015.isnull().sum()
missing_2016 = happiness2016.isnull().sum()
missing_2017 = happiness2017.isnull().sum()

# %%

combined = pd.concat([happiness2015, happiness2016,
                      happiness2017], ignore_index=True, sort=False)

# %%
combined.isnull().sum()

# %%

combined_updated = combined.set_index('year')
sns.heatmap(combined_updated.isnull(), cbar=False)

# %%
# identificamos que la lista de 2017 no tiene regiones, usamos la lista del
# 2015 que ya está unificada con la de 2016

regions = happiness2015.loc[:, ['country', 'region']]

# %%
# combinamos las regiones con el df y eliminamos

combined = pd.merge(left=combined, right=regions, on='country', how='left')
combined = combined.drop('region_x', axis=1)
combined.rename({'region_y': 'region'}, inplace=True, axis=1)

# Con esto todos los registros ya tienen región

# %%
# Revisamos datos duplicados antes de continuar

dups = combined.duplicated(['country', 'year'])
combined[dups]

# No existen datos duplicados, unificamos los nombres de los países para
# encontrar que existen países con mismo nombre pero en mayúsculas o minúsculas

combined['country'] = combined['country'].str.upper()
dups = combined.duplicated(['country', 'year'])
dups_r = combined[dups]

# %%
# Eliminamos los duplicados, verificamos previamente que sea seguro, al tomar
# el primer registro de los duplicados
combined = combined.drop_duplicates(['country', 'year'])

# %%
combined.isnull().sum()

# %%
# Eliminamos las columnas que no necesitas para nuestro análisis

columns_to_drop = ['lower_confidence_interval', 'standard_error',
                   'upper_confidence_interval', 'whisker_high', 'whisker_low']

combined = combined.drop(columns_to_drop, axis = 1)
missing = combined.isnull().sum()

# %%
missing

#%%
sorted = combined.set_index('region').sort_values(['region', 'happiness_score'])
sns.heatmap(sorted.isnull(), cbar=False)

#%%
# Después de ver el mapa de calor anterior, podemos asumir que remplazar los datos
# faltantes con la media de los demás, es seguro para nuestro análisis.

happiness_mean = combined['happiness_score'].mean()
print(happiness_mean)
combined['happiness_score_updated'] = combined['happiness_score'].fillna(happiness_mean)
print(combined['happiness_score_updated'].mean())

#%%
combined.loc[combined['happiness_score'].notnull(),'happiness_score']
#%%
sns.distplot(combined.loc[combined['happiness_score'].notnull(),'happiness_score'])
#%%
sns.distplot(combined.loc[combined['happiness_score'].notnull(),'happiness_score_updated'])
#%%
# Finalmente vemos que la distribución de los datos no se ve afectada por los valores nulos
# así que optamos por eliminarlos
combined = combined.dropna()
missing = combined.isnull().sum()

#%%
missing

#%%
