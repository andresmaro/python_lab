# %%
import os

import numpy as np
import pandas as pd

try:
    os.chdir(os.path.join(os.getcwd(), 'c_dataquest/g_data_cleaning'))
    print(os.getcwd())
except:
    pass

# %%

happiness2015 = pd.read_csv("data/World_Happiness_2015.csv")
happiness2016 = pd.read_csv("data/World_Happiness_2016.csv")
happiness2017 = pd.read_csv("data/World_Happiness_2017.csv")

happiness2015['Year'] = 2015
happiness2016['Year'] = 2016
happiness2017['Year'] = 2017
# %%

head_2015 = happiness2015[['Country', 'Happiness Score', 'Year']].head(3)
head_2016 = happiness2016[['Country', 'Happiness Score', 'Year']].head(3)

concat_axis0 = pd.concat([head_2015, head_2016])
print(concat_axis0)
concat_axis1 = pd.concat([head_2015, head_2016], axis=1)
print(concat_axis1)
# %%

concat_update_index = pd.concat([head_2015, head_2016], ignore_index=True)
concat_update_index

# %%

three_2015 = happiness2015[['Country', 'Happiness Rank', 'Year']].iloc[2:5]
three_2016 = happiness2016[['Country', 'Happiness Rank', 'Year']].iloc[2:5]

merged = pd.merge(three_2015, three_2016, on='Country')
merged

# %%

merged_left = pd.merge(three_2015, three_2016, how='left', on='Country')
merged_left_updated = pd.merge(
    three_2016, three_2015, how='left', on='Country')


# %%

merged_suffixes = pd.merge(left=three_2015, right=three_2016,
                           how='left', on='Country', suffixes=('_2015', '_2016'))
merged_updated_suffixes = pd.merge(
    left=three_2016, right=three_2015, how='left', on='Country', suffixes=('_2016', '_2015'))


# %%
four_2015 = happiness2015[['Country', 'Happiness Rank', 'Year']].iloc[2:6]
three_2016 = happiness2016[['Country', 'Happiness Rank', 'Year']].iloc[2:5]

merge_index = pd.merge(four_2015, right=three_2016,
                       left_index=True, right_index=True, suffixes=('_2015', '_2016'))
merge_index_left = pd.merge(left=four_2015, right=three_2016, how='left',
                            left_index=True, right_index=True, suffixes=('_2015', '_2016'))

# %%


happiness2017.rename(
    columns={'Happiness.Score': 'Happiness Score'},
    inplace=True)
combined = pd.concat([happiness2015, happiness2016, happiness2017], sort=False)
pivot_table_combined = combined.pivot_table(
    index='Year',
    values='Happiness Score',
    aggfunc=np.mean
)
pivot_table_combined.plot.barh(
    title='Mean Happiness Scores by Year',
    xlim=(0, 10))

# %%
