# %%
import seaborn as sns
import os

import pandas as pd
import numpy as np

try:
    os.chdir(os.path.join(os.getcwd(), 'c_dataquest/g_data_cleaning'))
    print(os.getcwd())
except:
    pass

# %%
dete_survey = pd.read_csv("data/dete_survey.csv")
tafe_survey = pd.read_csv("data/tafe_survey.csv", na_values='Not Stated')

# %%
dete_survey.head()
# %%
tafe_survey.head()
# %%
(dete_survey.info(), tafe_survey.info())
# %%
dete_survey.isnull().value_counts()
# %%
dete_survey_updated = dete_survey.drop(dete_survey.columns[28:49], axis=1)
tafe_survey_updated = tafe_survey.drop(tafe_survey.columns[17:66], axis=1)
# %%
print(dete_survey_updated.columns)
print(tafe_survey_updated.columns)

# %%

# Clean the column names
dete_survey_updated.columns = dete_survey_updated.columns.str.lower(
).str.strip().str.replace(' ', '_')

# Check that the column names were updated correctly
dete_survey_updated.columns


# Update column names to match the names in dete_survey_updated
mapping = {'Record ID': 'id', 'CESSATION YEAR': 'cease_date',
           'Reason for ceasing employment': 'separationtype', 'Gender. What is your Gender?': 'gender', 'CurrentAge. Current Age': 'age',
           'Employment Type. Employment Type': 'employment_status',
           'Classification. Classification': 'position',
           'LengthofServiceOverall. Overall Length of Service at Institute (in years)': 'institute_service',
           'LengthofServiceCurrent. Length of Service at current workplace (in years)': 'role_service'}
tafe_survey_updated = tafe_survey_updated.rename(mapping, axis=1)

# Check that the specified column names were updated correctly
tafe_survey_updated.columns

# %% [markdown]
# Para el estudio necesitamos los datos por Renuncia (Resignation)
# %%
tafe_survey_updated['separationtype'].value_counts()

# %% [markdown]
# Los datos dete, tiene varios campos de Resignation
# %%
dete_survey_updated['separationtype'].value_counts()
# %%

# Update all separation types containing the word "resignation" to 'Resignation'
dete_survey_updated['separationtype'] = dete_survey_updated['separationtype'].str.split(
    '-').str[0]

# Check the values in the separationtype column were updated correctly
dete_survey_updated['separationtype'].value_counts()

# %%
# Nos quedamos solo con los datos de Renuncia
dete_resignations = dete_survey_updated[dete_survey_updated['separationtype'] == 'Resignation'].copy(
)
tafe_resignations = tafe_survey_updated[tafe_survey_updated['separationtype'] == 'Resignation'].copy(
)

# Finalización: https://github.com/dataquestio/solutions/blob/master/Mission348Solutions.ipynb


#%%
