# %%
import os

import pandas as pd

try:
    os.chdir(os.path.join(os.getcwd(), 'c_dataquest/g_data_cleaning'))
    print(os.getcwd())
except:
    pass


# %%
mapping = {'Economy (GDP per Capita)': 'Economy',
           'Health (Life Expectancy)': 'Health', 'Trust (Government Corruption)': 'Trust'}
happiness2015 = pd.read_csv("data/World_Happiness_2015.csv")
happiness2015 = happiness2015.rename(mapping, axis=1)

happiness2015.head()

# %%


def label(element):
    if element > 1:
        return 'High'
    else:
        return 'Low'


economy_impact_map = happiness2015['Economy'].map(label)
economy_impact_map
# %%
economy_impact_apply = happiness2015['Economy'].apply(label)
economy_impact_apply

# %%

equal = economy_impact_map.equals(economy_impact_apply)
equal


# %%

def label(element, x):
    if element > x:
        return 'High'
    else:
        return 'Low'


economy_impact_apply = happiness2015['Economy'].apply(label, x=0.8)


# %%

def label(element):
    if element > 0.8:
        return 'High'
    else:
        return 'Low'


factors = ['Economy', 'Family', 'Health', 'Freedom', 'Trust', 'Generosity']

factors_impact = happiness2015[factors].applymap(label)
factors_impact.head(10)


# %%

def v_counts(col):
    num = col.value_counts()
    den = col.size
    return num/den


# &&

factors_impact.apply(pd.value_counts)

# %%

v_counts_pct = factors_impact.apply(v_counts)
v_counts_pct

# %%
factors = ['Economy', 'Family', 'Health', 'Freedom',
           'Trust', 'Generosity', 'Dystopia Residual']


def percentages(col):
    div = col/happiness2015['Happiness Score']
    return div * 100


factor_percentages = happiness2015[factors].apply(percentages)
factor_percentages


# %%
main_cols = ['Country', 'Region', 'Happiness Rank', 'Happiness Score']
factors = ['Economy', 'Family', 'Health', 'Freedom',
           'Trust', 'Generosity', 'Dystopia Residual']
melt = happiness2015.melt(main_cols, factors,)

# %%
melt['Percentage'] = ((melt['value']/melt['Happiness Score'])*100).round(2)

# %%

pv_melt = melt.pivot_table(index='variable', values='value')
pv_melt.plot.pie(y='value', x='variable', legend=False)
# %%
