# %%
import numpy as np
import os
try:
    os.chdir(os.path.join(os.getcwd(), 'c_dataquest/g_data_cleaning'))
    print(os.getcwd())
except:
    pass
# %%
from jupyterthemes import jtplot
import pandas as pd

jtplot.style()

happiness2015 = pd.read_csv('data/World_Happiness_2015.csv')
happiness2015.head()


# %%
mean_happiness = {}

for region in happiness2015['Region'].unique():
    mean = happiness2015.loc[happiness2015['Region']
                             == region, 'Happiness Score'].mean()
    mean_happiness[region] = mean

mean_happiness
# %%

grouped = happiness2015.groupby('Region')
aus_nz = grouped.get_group('Australia and New Zealand')
aus_nz
# %%

na_group = grouped.get_group('North America')
north_america = happiness2015.loc[happiness2015['Region']
                                  == 'North America', :]
equal = na_group == north_america

# %%

grouped.size()


# %%

means = grouped.mean()
means

# %%

happy_grouped = grouped['Happiness Score']
happy_mean = happy_grouped.mean()
happy_mean
# %%

grouped = happiness2015.groupby('Region')
happy_grouped = grouped['Happiness Score']


def dif(group):
    return (group.max() - group.mean())


happy_mean_max = happy_grouped.agg([np.mean, np.max])

mean_max_dif = happy_grouped.agg(dif)


# %%

happiness_means = happiness2015.groupby('Region')['Happiness Score'].mean()
happiness_means.plot.barh()

# %%
pv_happiness = happiness2015.pivot_table(
    values='Happiness Score', index='Region', aggfunc=np.mean, margins=True)
pv_happiness.plot.barh(
    xlim=(0, 10), title='Mean Happiness Scores by Region', legend=False)

world_mean_happiness = happiness2015['Happiness Score'].mean()
world_mean_happiness
# %%
happiness2015.pivot_table('Happiness Score', 'Region', aggfunc=[
                          np.mean, np.min, np.max], margins=True).plot.barh(xlim=(0, 10))

# %%

grouped = happiness2015.groupby('Region')[['Happiness Score', 'Family']]
happy_family_stats = grouped.agg([np.min, np.max, np.mean])
pv_happy_family_stats = happiness2015.pivot_table(
    ['Happiness Score', 'Family'], 'Region',
    aggfunc=[np.min, np.max, np.mean], margins=True)

pv_happy_family_stats

# %%
