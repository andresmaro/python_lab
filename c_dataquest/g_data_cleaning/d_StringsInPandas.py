# %%
import os

import pandas as pd

try:
    os.chdir(os.path.join(os.getcwd(), 'c_dataquest/g_data_cleaning'))
    print(os.getcwd())
except:
    pass

#%%
happiness2015 = pd.read_csv("data/World_Happiness_2015.csv")
world_dev = pd.read_csv("data/World_dev.csv")

happiness2015.head(2)
#%%
world_dev.head(2)
#%%
col_renaming = {'SourceOfMostRecentIncomeAndExpenditureData': 'IESurvey'}
merged = pd.merge(happiness2015,world_dev,how='left',left_on='Country',right_on='ShortName')
merged = merged.rename(col_renaming,axis=1)
merged.iloc[0]

#%%

def extract_last_word(element):
  el =str(element).split()
  return el[-1]

merged['Currency Apply'] = merged['CurrencyUnit'].apply(extract_last_word)
merged.head()

#%% [markdown]
### La versión vectorizada de la celda anterior. Más óptima. A continuación.
# - Better performance
# - Code that is easier to read and write

#%%
merged['Currency Vectorized'] = merged['CurrencyUnit'].str.split().str.get(-1)
merged['Currency Vectorized'].head()

#%%
# Cálculo de la longitud del campo
merged[merged['CurrencyUnit'].isnull()]
def compute_lengths(element):
    return len(str(element))
# Teniendo en cuenta que existen 13 valores NaN
merged['CurrencyUnit'].isnull().sum()
# El resultado es que la funcion compute_lengths calcula NaN como string
lengths_apply = merged['CurrencyUnit'].apply(compute_lengths)

#%%
# Los métodos vectorizados exluyen los valores nulos
lengths = merged['CurrencyUnit'].str.len()
value_counts = lengths.value_counts(dropna=False)
value_counts


#%%
pattern = r"[Nn]ational accounts"
national_accounts = merged['SpecialNotes'].str.contains(pattern)
national_accounts.value_counts(dropna=False)

#%%


#%%
# Para evitar el error anterior de:
# merged[national_accounts]
# Al contener valores nulos NaN, pasamos el parametro na en falso
national_accounts = merged['SpecialNotes'].str.contains(pattern, na=False)
merged_national_accounts = merged[national_accounts]
merged_national_accounts.head()

#%%

pattern = r"(?P<Years>[1-2][0-9]{3})"
years = merged['IESurvey'].str.extractall(pattern)
value_counts = years['Years'].value_counts()
print(value_counts)

#%%

pattern = r"(?P<First_Year>[1-2][0-9]{3})/?(?P<Second_Year>[0-9]{2})?"
years = merged['IESurvey'].str.extractall(pattern)
first_two_year = years['First_Year'].str[0:2]
years['Second_Year'] = first_two_year + years['Second_Year']
years


#%%
merged['IncomeGroup'] = merged['IncomeGroup'].str.replace(' income', '').str.replace(':', '').str.upper()
pv_incomes = merged.pivot_table(values='Happiness Score', index='IncomeGroup')
pv_incomes.plot(kind='bar', rot=30, ylim=(0,10))
plt.show()

#%%
