from os import path
from csv import reader
import numpy as np

script_dir = path.dirname(__file__)
rel_path = "data/nyc_taxis.csv"
file = open(path.join(script_dir, rel_path), encoding="utf8")

taxi_list = list(reader(file))

taxi_list = taxi_list[1:]

print(taxi_list[0])

# convert all values to floats
converted_taxi_list = []
for row in taxi_list:
    converted_row = []
    for item in row:
        converted_row.append(float(item, ))
    converted_taxi_list.append(converted_row)

taxi = np.array(converted_taxi_list)

# Calculo de la velocidad del viaje en MPH
trip_distance_miles = taxi[:, 7]
trip_length_seconds = taxi[:, 8]

trip_length_hours = trip_length_seconds / 3600
trip_mph = trip_distance_miles / trip_length_hours

mph_min = trip_mph.min()  # mínimo
mph_max = trip_mph.max()  # máximo
mph_mean = trip_mph.mean()  # promedio

print({'min': mph_min, 'max': mph_max, 'promedio': mph_mean})

taxi_column_means = taxi.mean(axis=0)

trip_mph_2d = np.expand_dims(trip_mph, axis=1)
taxi = np.concatenate([taxi, trip_mph_2d], axis=1)

# Ordenar por MPH
column_sort = np.argsort(taxi[:, 15])
taxi_sorted = taxi[column_sort]

print(taxi_sorted[-5:])
