from csv import reader
import os

script_dir = os.path.dirname(__file__)
rel_path = "data/AppleStore.csv"
opened_file = open(os.path.join(script_dir, rel_path))

read_file = reader(opened_file)
apps_data = list(read_file)
data_sizes = []
for row in apps_data[1:]:
    size = float(row[3])
    data_sizes.append(size)

min_size = min(data_sizes)
max_size = max(data_sizes)

print(min_size, max_size)
