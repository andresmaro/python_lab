# My comment
print("\n\x1b[36m****  COMPREHENSIONS  ****\x1b[0m")
print("\n\x1b[36m****  GENERATOR  ****\x1b[0m")


def gen123():
    yield 1
    yield 2
    yield 3


g = gen123()

next(g)
print("".join(["next: ", str(next(g))]))


def contador(maximo, iterable):
    actual = 0
    for item in iterable:
        if actual == maximo:
            return
        actual += 1
        yield item


def run_contador():
    lista = [2, 4, 6, 8, 10]
    for item in contador(2, lista):
        print(item)


def run_w(num):
    n = 0
    while n <= num:
        print(n)
        n += 1


run_contador()
