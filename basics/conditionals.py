menu = """
Welcome to the currency converter 🪙
1 - COP
2 - MXN
3 - PEN

Please choose an option:
"""
option = int(input(menu))

if option == 1:
    local = input('How many Pesos do you have?\n')
    change = 3560
elif option == 2:
    local = input('How many Pesos do you have?\n')
    change = 19.91
elif option == 3:
    local = input('How many Soles do you have?\n')
    change = 3.82
else:
    print('Please choose an option from the list')

local = float(local)
usd = round(local / change)
usd = str(usd)
print(f'You have U$D {usd}')