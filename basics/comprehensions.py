def main():
    elements = [i for i in range(1, 101) if i % 3 == 0]
    print(len(elements))
    print(elements)

    dictionary = {i: i ** 3 for i in range(1, 101) if i % 3 == 0}
    print([i for i in dictionary])
    print([i for i in dictionary.values()])
    print([i for i in dictionary.items()])


if __name__ == '__main__':
    main()
