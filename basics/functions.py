from random import sample


def add_persona(persona):
    select.append(persona)


base = [
    {"id": 1, "nombre": "Andres"},
    {"id": 2, "nombre": "Liliana"},
    {"id": 3, "nombre": "Juan"},
    {"id": 4, "nombre": "Laura"},
    {"id": 5, "nombre": "Andrea"},
    {"id": 6, "nombre": "Jairo"},
    {"id": 7, "nombre": "Melissa"},
    {"id": 8, "nombre": "Daniel"},
]

select = []

num = int(input("Personas a generar: "))

rango = sample(range(8), num)

for p in rango:
    add_persona(base[p])

print(select)


# multiple arguments
# *args
def print_list(*params):
    for i in params:
        print(i)


# *kwargs
def print_keys(**p):
    for i in p:
        print(f"{i}: {p[i]}")


# local functions
def enclose():
    pi = 3.14159265359

    def calc(n):
        return n * pi  # this closure mantain pi value for further calls

    return calc


# >>> e = enclose()
# >>> e(3)
# 9.424777960770001

# Functions factories
def raise_to(n):
    def exp(x):
        return pow(x, n)

    return exp


# >>> squared = raise_to(2)
# >>> squared(2)
# 4
# >>> squared(3)
# 6
# >>> cubed = raise_to(3)
# >>> cubed(2)
# 8
# >>> cubed(3)
# 27

# *****+ DECORATORS *******

def escape_unicode(f):
    def wrap(*args, **kwargs):
        x = f(*args, **kwargs)
        return ascii(x)

    return wrap

@escape_unicode
def local_city():
    return "Él viñedo"
