# My comment
foo1: int = 5

print(f"This is a custom string {foo1}")
verdadero = True
falso = False
str(1)
int("2")

foo2 = "uno" if falso else "dos"
print(foo2)

if not falso:
    print("Es verdadero \n - muy verdadero")

print("\n\x1b[36m****  LISTS  ****\x1b[0m")

nums = ["one", 'two', "three"]
print(nums[1])  # second element

nums[1] = "two"  # override
print(nums[1])

nums.append("four")
nums.append("five")
print(nums[-1])  # first element from right to left
print("four" in nums)
print(f"length: {len(nums)}")
print(nums[1:])  # slicing *temporal
print(nums[1:-2])
print(nums)

print("\n\x1b[36m****  TUPLES  ****\x1b[0m")

fruits = ("apple", "banana", "cherry")
print(fruits[2])
print(fruits[1:])
(red, yellow, purple) = fruits
print(red)
for fruit in fruits[:1] * 2:
    print(fruit)

print("\n\x1b[36m****  LOOPS  ****\x1b[0m")

for num in nums[2:]:
    print(f"número: {num}")

x = 0
while x < 3:
    print(f"count: {x}")
    x += 1

print("\n\x1b[36m****  DICTIONARIES  ****\x1b[0m")

romanos = {"uno": "I", "dos": "II", "tres": "III", "cuatro": "IV", "cinco": "V"}
print(romanos["cuatro"])
print(romanos.get("seis", "VI"))  # evita KeyError
print(romanos.keys())
print(romanos.values())
del romanos["cinco"]
print(romanos)
