#!/bin/bash
#Start with docker image loaded
apt update
apt install build-essential -y

#Oh-My-Zsh
apt install zsh -y
apt-get install powerline fonts-powerline -y
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
chsh -s /bin/zsh
rm ~/.zshrc
cp .zshrc ~/

#GIT config
git config --global user.name "@andresmaro"
git config --global user.email andresmaro@gmail.com

#docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
